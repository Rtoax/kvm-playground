# Starting a VM

In this exercise we execute a simple program in a VM.

The exercise introduces basic KVM APIs and just enough basics about CPU to be able to run a program.
